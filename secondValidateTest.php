require_once __DIR__ . '/secondValidate.php';

class SecondValidateTest extends PHPUnit_Framework_TestCase{
    public function testSecondValidateAsPhone(){
        //valid
        $this->assertTrue(SecondValidate::ValidateAsPhone('0120-444-444'));
        $this->assertTrue(SecondValidate::ValidateAsPhone('090-0000-1111'));
        $this->assertTrue(SecondValidate::ValidateAsPhone('03-5614-0808'));
        $this->assertTrue(SecondValidate::ValidateAsPhone('011-000-1111'));
        $this->assertTrue(SecondValidate::ValidateAsPhone('0143-00-1111'));
        $this->assertTrue(SecondValidate::ValidateAsPhone('01454-0-1111'));

        //invalid

        $this->assertFalse(SecondValidate::ValidateAsPhone('0120-44-444'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('090-000-1111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('03-561-0808'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('011-00-1111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('0143-0-1111'));

        $this->assertFalse(SecondValidate::ValidateAsPhone('0120-444-4444'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('090-0000-11111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('03-5614-08081'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('011-000-11111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('0143-00-11111'));


        $this->assertFalse(SecondValidate::ValidateAsPhone('1200-444-444'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('900-0000-1111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('30-5614-0808'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('110-000-1111'));
        $this->assertFalse(SecondValidate::ValidateAsPhone('1430-00-1111'));

    }
}
